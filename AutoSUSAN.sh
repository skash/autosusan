#! /bin/bash

Usage() {
    echo ""
    echo "Usage: AutoSUSAN <3D_input> <sigma HWHM (optional)>"
    echo ""
    exit 1
}

if [ "$1" = "" ] || [ "$1" = "-h" ]
then
Usage
fi

# get robust range
echo "---------------- AutoSUSAN --------------------"
echo "++++ Identifying robust range ..."
robust_range=`fslstats $1 -r`
# make string to array
robust_range=($robust_range)
# get vals
robust_min=${robust_range[0]}
robust_max=${robust_range[1]}
echo "++++ Intensity mininum = $robust_min"
echo "++++ Intensity maximum = $robust_max"

brightness_threshold=`awk "BEGIN {robust_diff=$robust_max-$robust_min; bt=robust_diff*0.1; print bt}"`

echo "++++ Brightness threshold is $brightness_threshold"

if [ -z "$2" ]; then
echo "++++ Running SUSAN with 0 HWHM ..."
susan $1 $brightness_threshold 0 3 1 0 ${1%.*.*}_susan
else
echo "++++ Running SUSAN with $2 HWHM ..."
susan $1 $brightness_threshold $2 3 1 0 ${1%.*.*}_susan
fi

echo "++++ Done"
echo "---------------- AutoSUSAN --------------------"